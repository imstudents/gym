/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.im.gms.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Industrial Master
 */
public class DateUtils {
    /**
     * 
     * @param date
     * @return Return the Given Date in yyyy-MM-dd format
     */
    public static String getDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
    
    
    /**
     * 
     * @param date
     * @return Return the Given Date in hh:mm:ss format
     */
    public static String getTime(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        return sdf.format(date);
    }
    
}
