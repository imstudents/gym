/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.im.gms.util;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Industrial Master
 */
public class NicUtils {
    /**
     * 
     * @param nic
     * @return  Age of Given NIC Holder
     */
    public static int getAge(String nic){
        String yearText = nic.substring(0, 2);
        int year = Integer.parseInt(yearText);
        Date today = new Date();
        int thisYear = today.getYear();
        int age = thisYear-year;
        return age;
    }
    
    /**
     * 
     * @param nic
     * @return  BirthDay of Given NIC Holder
     */
    public static String getBirthDay(String nic){
        String yearText = nic.substring(0, 2);
        int year = Integer.parseInt(yearText)+1900;
        String dayText = nic.substring(2, 5);
        int day = Integer.parseInt(dayText);
        if(day>=500){
            day-=500;
        }
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_YEAR, day);
        String bDay = DateUtils.getDate(cal.getTime());
        return bDay;
    }
    
}
