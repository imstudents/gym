/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.im.gms.control;

import com.im.gms.model.Member;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;

/**
 *
 * @author Industrial Master
 */
public class MemberController {
    
    public static boolean save(Member member){
        boolean saved = false;
        //4. DB Connection
        String url = "jdbc:mysql://localhost:3306/gym";
        String un = "root";
        String pw= "123";
        String driverName = "com.mysql.jdbc.Driver";
        
        try{
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(url, un, pw);
            
            String sql = "INSERT INTO member (name,nic,age,gender,address, mobile, "
                    + "join_date, added_Date, added_time, status) VALUES (?,?,?,?,?,?,?,?,?,?)";
            
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, member.getName());
            ps.setString(2, member.getNic());
            ps.setInt(3, member.getAge());
            ps.setString(4, member.getGender());
            ps.setString(5, member.getAddress());
            ps.setInt(6, member.getMobile());
            ps.setDate(7, new java.sql.Date(member.getJoinDate().getTime()));
            ps.setDate(8, new java.sql.Date(member.getAddedDate().getTime()));
            ps.setDate(9, new java.sql.Date(member.getAddedDate().getTime()));
            ps.setBoolean(10, member.isStatus());
            
            ps.executeUpdate();
            saved = true;
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return saved;
    }
    
}
